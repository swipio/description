<div align="center">
  <img src=".gitlab/logo.svg" height="124px"/><br/>
  <h1> Swipio description </h1>
  <p>Full description of the <a href="https://gitlab.com/swipio">swipio project</a></p>
</div>

## 📝 About The Project
In a large group of friends, it is always difficult to choose a movie to watch that will suit everyone. This project aims to solve this problem. A group of friends individually scrolls through the list of available movies and uses swipe gestures to determine which movies they like. After that, the system gives out a list of films that can be watched, in order of rating (at the very top is the film that more friends liked)

The project is split into three repositories:

* [**Mobile app**](https://gitlab.com/swipio/mobile_app) - mobile app for Android and iOS

* [**Backend**](https://gitlab.com/swipio/backend) - backend with API access

* [**File storage**](https://gitlab.com/swipio/file_storage) - file storage with API access (used to store images of movies)

## ⚡️ Try Swipio!

[Download](https://gitlab.com/swipio/mobile_app/-/jobs/artifacts/master/file/app.apk?job=build) and install latest APK for Android

## Info to make README look professional

### Requirements

We used **Rational Unified Process** (RUP) to develop our system. We set up the requirements and continuously developed app to fit these requirements.

See full description of the process :link: [**here**](docs/requirements.pdf) (glossary, stakeholders roles, user stories, non-functional requirements)

### Design

*Mobile application* is written using crossplatform **Flutter** framework for **Dart** language. We used **BloC** Architecture to fit our nonfunctional requirements and make our system structurized, maintainable and testable. See :link: [**mobile design**](docs/mobile_design.pdf) for more info (UML Class and Sequence diagrams, SOLID and Design patterns).

*Backend* is written in FastAPI python framework. We divided all instances into Endpoints, Services and Repositories. See :link: [**backend design**](docs/backend_design.pdf) for more info.

### Architecture

Static view Diagram

![](.gitlab/static_arch.png)

Dynamic view  diagram

![](.gitlab/dynamic_arch.png)

Clients that use mobile application connect to Main backend to login, signup, upload films, group data, vote for films

Clients use file storage module to download files  - media content

### Code

See a full working demo of project via [**this link**](https://pollen-anger-934.notion.site/Swipio-App-Demo-f0ae21311fd344129b043fc5d6cc2369)

For the *backend* and *file storage* we use 4 static analyzers (linetrs): [flake8](https://flake8.pycqa.org/en/latest/), [pylint](https://pylint.pycqa.org/en/latest/), [mypy](https://mypy.readthedocs.io/en/stable/) (static type checker), [black](https://black.readthedocs.io/en/stable/) (formatter with check method)

![](.gitlab/linters.png)

For the *mobile app* we use standard static analyzer (linter)

![](.gitlab/mobile_linter.png)

For the *backend* and *file storage* we use pytest to test our application and measure coverage (76% right now)

![](.gitlab/test.png)

### CI/CD

We developed pipelines for CI/CD for the backend, file storage and mobile app

Here is the pipeline for the backend and file storage:
![CI/CD pipeline](.gitlab/backend_pipeline.png)

- **1st stage:** construction of  develop version of the project, launching of linkers and tests

- **2nd stage:** construction and deployment  production docker-image for both architectures: amd64, arm64

- **3rd stage:** tagging published image with "latest" tag (only when merging to the master)

- **4th stage:** continuous deployment to the staging and  production servers

The pipeline for a mobile application is a bit simpler and consists of only one stage - building the APK for the android. [Here](https://gitlab.com/swipio/mobile_app/-/jobs/artifacts/master/file/app.apk?job=build) you can find the latest build artifact.

### Github stars

We see our application as a product, not as an open-source solution, but we have a small part of our project which may be an open-source library. We’ve developed our own file storage. It’s lightweight and is a good solution for small projects.

[Link to the file storage](https://github.com/Markovvn1/swipio-file-storage) (26 stars now)

## :computer: Contributors

<p>

  :mortar_board: <i>All participants in this project are undergraduate students in the <a href="https://apply.innopolis.university/en/bachelor/">Department of Computer Science</a> <b>@</b> <a href="https://innopolis.university/">Innopolis University</a></i>. <br> <br>

  :boy: <b>Vladimir Markov</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>v.markov@innopolis.university</a> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GitLab: <a href="https://gitlab.com/markovvn1">@markovvn1</a> <br>

  :boy: <b>Gleb Osotov</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>g.osotov@innopolis.university</a> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GitHub: <a href="https://github.com/glebosotov">@glebosotov</a> <br>

  :boy: <b>Dmitriy Tsaplya</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>d.tsaplya@innopolis.university</a> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GitLab: <a href="https://github.com/tsaplyadmitriy">@tsaplyadmitriy</a> <br>

  :girl: <b>Polina Minina</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>p.minina@innopolis.university</a> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GitLab: <a href="https://github.com/pminina01">@pminina01</a> <br>
</p>
